﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;
        private bool enemyMove;
        private void Update()
        {
            Move();
            enemySpaceship.Fire();
        }

         private void Move()
         {
             if (transform.position.x >= 8f)
             {
                 enemyMove = false;
             }

             if (transform.position.x <= -8f)
             {
                 enemyMove = true;
             }

             if (enemyMove)
             {
                 transform.position = new Vector2(transform.position.x + enemySpaceship.Speed * Time.deltaTime, transform.position.y);
             }
             else
             {
                 transform.position = new Vector2(transform.position.x - enemySpaceship.Speed * Time.deltaTime,transform.position.y);
             }
         }
    }    
}

