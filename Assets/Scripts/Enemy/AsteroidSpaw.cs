using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpaw : MonoBehaviour
{
    [SerializeField] private float min_x = -8f, max_X = 8f;

    public GameObject[] asteroid;

    [SerializeField] private float timer = 2f;
    
    // Start is called before the first frame update
    private void Start()
    {
        Invoke("SpawnAsteroid",timer);
    }
    private void SpawnAsteroid()
    {
        float pos_X = Random.Range(min_x, max_X);
        Vector3 temp = transform.position;
        temp.x = pos_X;

        Instantiate(asteroid[Random.Range(0, asteroid.Length)], temp, Quaternion.identity);

        Invoke("SpawnAsteroid",timer);
    }
    
}
