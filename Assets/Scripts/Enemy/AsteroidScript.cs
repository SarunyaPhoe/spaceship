using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using Spaceship;
using UnityEngine;
using Random = UnityEngine.Random;

public class AsteroidScript : MonoBehaviour
{
    [SerializeField] private int damage;
    
    [SerializeField] private float speed = 5f;
    [SerializeField] private float rotateSpeed = 50f;

    [SerializeField] private float deactivateTimer = 3f;
    
    // Start is called before the first frame update
    private void Start()
    {
        Invoke("DeactvateGameObject",deactivateTimer);
        
        if (Random.Range(0,2) > 0)
        {
            rotateSpeed = Random.Range(rotateSpeed, rotateSpeed + 20f);
            rotateSpeed *= -1f;
        }
    }

    // Update is called once per frame
    private void Update()
    {
        Move();
        RotateEnemy();
    }

    private void Move()
    {
        Vector3 temp = transform.position;
        temp.y -= speed * Time.deltaTime;
        transform.position = temp;
    }

    private void RotateEnemy()
    {
        transform.Rotate(new Vector3(0f,0f,rotateSpeed * Time.deltaTime), Space.World);
    }

    private void DeactvateGameObject()
    {
        Destroy(gameObject);
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        var target = other.gameObject.GetComponent<IDamagable>();
        target?.TakeHit(damage);
    }
}
