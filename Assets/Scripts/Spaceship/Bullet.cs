using System;
using UnityEngine;

namespace Spaceship
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private int damage;
        [SerializeField] private float speed;
        [SerializeField] private new Rigidbody2D rigidbody2D;

        [SerializeField] private float deactivateTimer = 1f;

        private AudioSource bulletAudio;
        public void Init(Vector2 diretion)
        {
            Move(diretion);
        }

        private void Start()
        {
            Invoke("DeactvateGameObject",deactivateTimer);
        }

        private void Awake()
        {
            Debug.Assert(rigidbody2D != null, "rigidbody2D cannot be null");
            bulletAudio = GetComponent<AudioSource>();
            bulletAudio.Play();
        }

        private void Move(Vector2 diretion)
        {
            rigidbody2D.velocity = diretion * speed;
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            var target = other.gameObject.GetComponent<IDamagable>();
            target?.TakeHit(damage);
        }
        
        private void DeactvateGameObject()
        {
            Destroy(gameObject);
        }
    }
}