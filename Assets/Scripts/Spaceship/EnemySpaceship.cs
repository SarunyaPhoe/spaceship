using System;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private double fireRate = 1;
        private float fireCounter = 0;

        [SerializeField] private AudioClip enemyExplodedSound;
        [SerializeField] private float enemyExplodedVolume = 0.3f;
        
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            AudioSource.PlayClipAtPoint(enemyExplodedSound,Camera.main.transform.position,enemyExplodedVolume);
            Debug.Assert(Hp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                bullet.Init(Vector2.down);
                fireCounter = 0;
            }
        }
    }
}