using System;
using Spaceship;
using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class GameManagerLv2 : Singleton<GameManagerLv2>
    {
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private ScoreManager scoreManager;
        
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;
        
        public GameObject gameOverPanel;
        public Text gameOverText;
        public GameObject restartButton;
        public GameObject restartLv2Button;

        private void Awake()
        {
            restartButton.SetActive(false);
            restartLv2Button.SetActive(false);
            
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");
            
            gameOverPanel.SetActive(false);
        }
        
        void Start()
        {
            StartGame();
        }
        
        private void StartGame()
        {
            scoreManager.Init(this);
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }
        
        private void SpawnPlayerSpaceship()
        {
            var spaceship = Instantiate(playerSpaceship);
            spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
        }

        public void OnPlayerSpaceshipExploded()
        {
            DestroyRemainingShips();
            restartLv2Button.SetActive(true);
            gameOverPanel.SetActive(true);
            gameOverText.text = "Lose";
        }

        private void SpawnEnemySpaceship()
        {
            var spaceship = Instantiate(enemySpaceship);
            spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spaceship.OnExploded += OnEnemySpaceshipExploded;
        }

        private void OnEnemySpaceshipExploded()
        {
            scoreManager.SetScore(1);
            Restart();
        }

        private void Restart()
        {
            DestroyRemainingShips();
            WinGame();
        }

        private void DestroyRemainingShips()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }
            
            var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayer)
            {
                Destroy(player);
            }

            var remainingSpaw = GameObject.FindGameObjectsWithTag("Spaw");
            foreach (var spaw in remainingSpaw)
            {
                Destroy(spaw);
            }
            
            var remainingAsteroid = GameObject.FindGameObjectsWithTag("Asteroid");
            foreach (var asteroid in remainingAsteroid)
            {
                Destroy(asteroid);
            }
        }

        private void WinGame()
        {
            restartButton.SetActive(true);
            gameOverPanel.SetActive(true);
            gameOverText.text = "YOU WIN!";
        }
    }
}
